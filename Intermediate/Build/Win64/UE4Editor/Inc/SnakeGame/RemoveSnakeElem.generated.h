// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_RemoveSnakeElem_generated_h
#error "RemoveSnakeElem.generated.h already included, missing '#pragma once' in RemoveSnakeElem.h"
#endif
#define SNAKEGAME_RemoveSnakeElem_generated_h

#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARemoveSnakeElem(); \
	friend struct Z_Construct_UClass_ARemoveSnakeElem_Statics; \
public: \
	DECLARE_CLASS(ARemoveSnakeElem, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ARemoveSnakeElem) \
	virtual UObject* _getUObject() const override { return const_cast<ARemoveSnakeElem*>(this); }


#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARemoveSnakeElem(); \
	friend struct Z_Construct_UClass_ARemoveSnakeElem_Statics; \
public: \
	DECLARE_CLASS(ARemoveSnakeElem, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ARemoveSnakeElem) \
	virtual UObject* _getUObject() const override { return const_cast<ARemoveSnakeElem*>(this); }


#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARemoveSnakeElem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARemoveSnakeElem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARemoveSnakeElem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARemoveSnakeElem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARemoveSnakeElem(ARemoveSnakeElem&&); \
	NO_API ARemoveSnakeElem(const ARemoveSnakeElem&); \
public:


#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARemoveSnakeElem(ARemoveSnakeElem&&); \
	NO_API ARemoveSnakeElem(const ARemoveSnakeElem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARemoveSnakeElem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARemoveSnakeElem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARemoveSnakeElem)


#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_11_PROLOG
#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_INCLASS \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_RemoveSnakeElem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ARemoveSnakeElem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_RemoveSnakeElem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
