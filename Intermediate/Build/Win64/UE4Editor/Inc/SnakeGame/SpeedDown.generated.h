// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SpeedDown_generated_h
#error "SpeedDown.generated.h already included, missing '#pragma once' in SpeedDown.h"
#endif
#define SNAKEGAME_SpeedDown_generated_h

#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeedDown(); \
	friend struct Z_Construct_UClass_ASpeedDown_Statics; \
public: \
	DECLARE_CLASS(ASpeedDown, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeedDown) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedDown*>(this); }


#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASpeedDown(); \
	friend struct Z_Construct_UClass_ASpeedDown_Statics; \
public: \
	DECLARE_CLASS(ASpeedDown, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeedDown) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedDown*>(this); }


#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeedDown(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeedDown) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedDown); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedDown); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedDown(ASpeedDown&&); \
	NO_API ASpeedDown(const ASpeedDown&); \
public:


#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedDown(ASpeedDown&&); \
	NO_API ASpeedDown(const ASpeedDown&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedDown); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedDown); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeedDown)


#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_SpeedDown_h_11_PROLOG
#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_INCLASS \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_SpeedDown_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SpeedDown_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASpeedDown>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_SpeedDown_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
