// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/RemoveSnakeElem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoveSnakeElem() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ARemoveSnakeElem_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ARemoveSnakeElem();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ARemoveSnakeElem::StaticRegisterNativesARemoveSnakeElem()
	{
	}
	UClass* Z_Construct_UClass_ARemoveSnakeElem_NoRegister()
	{
		return ARemoveSnakeElem::StaticClass();
	}
	struct Z_Construct_UClass_ARemoveSnakeElem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARemoveSnakeElem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARemoveSnakeElem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RemoveSnakeElem.h" },
		{ "ModuleRelativePath", "RemoveSnakeElem.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ARemoveSnakeElem_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ARemoveSnakeElem, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARemoveSnakeElem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARemoveSnakeElem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARemoveSnakeElem_Statics::ClassParams = {
		&ARemoveSnakeElem::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARemoveSnakeElem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARemoveSnakeElem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARemoveSnakeElem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARemoveSnakeElem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARemoveSnakeElem, 4267629602);
	template<> SNAKEGAME_API UClass* StaticClass<ARemoveSnakeElem>()
	{
		return ARemoveSnakeElem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARemoveSnakeElem(Z_Construct_UClass_ARemoveSnakeElem, &ARemoveSnakeElem::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ARemoveSnakeElem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARemoveSnakeElem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
