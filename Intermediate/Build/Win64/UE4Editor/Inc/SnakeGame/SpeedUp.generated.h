// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SpeedUp_generated_h
#error "SpeedUp.generated.h already included, missing '#pragma once' in SpeedUp.h"
#endif
#define SNAKEGAME_SpeedUp_generated_h

#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeedUp(); \
	friend struct Z_Construct_UClass_ASpeedUp_Statics; \
public: \
	DECLARE_CLASS(ASpeedUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeedUp) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedUp*>(this); }


#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASpeedUp(); \
	friend struct Z_Construct_UClass_ASpeedUp_Statics; \
public: \
	DECLARE_CLASS(ASpeedUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeedUp) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedUp*>(this); }


#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeedUp(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeedUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedUp(ASpeedUp&&); \
	NO_API ASpeedUp(const ASpeedUp&); \
public:


#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedUp(ASpeedUp&&); \
	NO_API ASpeedUp(const ASpeedUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedUp); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeedUp)


#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_SpeedUp_h_11_PROLOG
#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_INCLASS \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_SpeedUp_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SpeedUp_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASpeedUp>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_SpeedUp_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
