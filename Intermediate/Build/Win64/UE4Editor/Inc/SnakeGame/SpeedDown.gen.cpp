// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/SpeedDown.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedDown() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ASpeedDown_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASpeedDown();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASpeedDown::StaticRegisterNativesASpeedDown()
	{
	}
	UClass* Z_Construct_UClass_ASpeedDown_NoRegister()
	{
		return ASpeedDown::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedDown_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedDown_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedDown_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedDown.h" },
		{ "ModuleRelativePath", "SpeedDown.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedDown_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedDown, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedDown_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedDown>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpeedDown_Statics::ClassParams = {
		&ASpeedDown::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedDown_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedDown_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedDown()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpeedDown_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpeedDown, 2313341275);
	template<> SNAKEGAME_API UClass* StaticClass<ASpeedDown>()
	{
		return ASpeedDown::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpeedDown(Z_Construct_UClass_ASpeedDown, &ASpeedDown::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ASpeedDown"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedDown);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
