// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			AFood::Destroy();
			SpawnFood();
		}
	}
}

void AFood::SpawnFood()
{
		int X = -620 + rand() % (620 - (-620));
		int Y = -620 + rand() % (620 - (-620));

		FVector NewLocation(X, Y, 0);
		FTransform NewTransform(NewLocation);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}

