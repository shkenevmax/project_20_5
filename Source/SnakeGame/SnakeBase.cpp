// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "Food.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 80.f;
	MovementSpeed = 1,0.f;
	LastMoveDirection = EMovementDirection::DOWN;
	SnakeTick = 9999;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	AddSnakeElement(4);

	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	ChangeSize(SnakeTick);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		
		NewSnakeElem->SnakeOwner = this;

		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	SnakeElements[SnakeElements.Num() - 1]->SetElemVisibility(false);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num()-1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[SnakeElements.Num() - 1]->SetElemVisibility(true);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::ChangeSize(int SnakeTicks)
{
	if (SnakeTicks == SnakeElements.Num())
	{
		FVector SmallScale(0.5, 0.5, 0.5);
		SnakeElements[SnakeTicks-1]->SetActorScale3D(SmallScale);
		SnakeTick = 9999;
	}
	else
	{
		if (SnakeElements.Num() > SnakeTicks)
		{
			FVector BigScale(0.7, 0.7, 0.7);
			FVector SmallScale(0.5, 0.5, 0.5);
			SnakeElements[SnakeTicks]->SetActorScale3D(BigScale);
			if (SnakeTicks > 0)
			{
				SnakeElements[SnakeTicks - 1]->SetActorScale3D(SmallScale);
			}
			SnakeTick++;
		}
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);

		SnakeTick = 0;

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::DeleteSnake()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
}

void ASnakeBase::ChangeSpeed(bool isSpeedUp)
{
	if (isSpeedUp)
	{
		if (MovementSpeed > 0.3f)
		{
			MovementSpeed -= 0.2f;
			SetActorTickInterval(MovementSpeed);
		}
	}
	else
	{
		MovementSpeed += 0.2f;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::RemoveSnakeElement()
{
	SnakeElements[SnakeElements.Num() - 1]->Destroy();
	SnakeElements.RemoveAt(SnakeElements.Num() - 1);
}

