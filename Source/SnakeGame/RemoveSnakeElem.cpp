// Fill out your copyright notice in the Description page of Project Settings.


#include "RemoveSnakeElem.h"

// Sets default values
ARemoveSnakeElem::ARemoveSnakeElem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARemoveSnakeElem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARemoveSnakeElem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARemoveSnakeElem::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->RemoveSnakeElement();
			ARemoveSnakeElem::Destroy();
		}
	}
}

